<?php
//Para comprobar que envia el POST.
/*echo "<pre>";
print_r($_POST);
echo "</pre>";
exit();*/
if (isset($_SESSION["user"])) {
		echo "<h3>Usuario: ".$_SESSION['user_name']."</h3>";
		echo "<a href='logout.php'>Cerrar sesion</a><br>";
	}else{
		header("Location: login.php");
		exit();
}
require_once "conf.php";
//Recoge la ciudad de BD si se pasa su identificador 
$id=null;
$name=null;
$countrycode=null;
$district=null;
$population=null;
if (isset($_POST["ID"]) && !empty($_POST["ID"])) {
	try{
		$stmt = $con->query('SELECT * FROM city');
	 	$stmt->execute();
	 	if ($datos=$stmt->fetch(PDO::FETCH_ASSOC)) {
	 		$id=$datos["ID"];
	 		$name=$datos["Name"];
	 		$countrycode=$datos["CountryCode"];
	 		$district=$datos["District"];
	 		$population=$datos["Population"];
	 	}
 	}catch(PDOException $e){
 		echo "ERROR ".$e->getMessage();
 	}
 }
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/form.css">
	<title>Document</title>
</head>
<body>
	<h1>Formulario</h1>
	<nav>
 		<form action="index.php" method="POST">
 			<input type="submit" name="Listado" value="Listado">
 		</form>
 	</nav>
 	<?php
 	if (isset($_GET["ID"])){
 	include_once "conf.php";
		$id=$_GET["ID"];
		$name=$_GET["Name"];
		$countrycode=$_GET["CountryCode"];
		$district=$_GET["District"];
		$population=$_GET["Population"];
		$stmt=$con->query("SELECT * FROM city WHERE ID=".$id);

		
 	?>
	<form action="post.php" method="POST">
		<fieldset>
			<legend>Ciudad</legend>
			ID: <input type="text" name="identificador" tabindex="1" readonly value="<?php echo $id ?>" />
			Nombre de la ciudad: <input type="text" name="nombre" id="nombre" tabindex="2" value="<?php echo $name ?>"/>
			Código de la ciudad: <input type="text" name="codigo" id="codigo"/ tabindex="3" value="<?php echo $countrycode ?>">
			Nombre del distrito: <input type="text" name="distrito" id="distrito" tabindex="4" value="<?php echo $district ?>"/>
			Número de habitantes: <input type="number" name="habitantes" id="habitantes" tabindex="5" value="<?php echo $population ?>"/>
			<input type="submit" name="guardar" value="Guardar">
			<input type="button" name="cancelar" value="Cancelar">
		</fieldset>
	</form>
<?php }else {?>
	<form action="post.php" method="POST">
		<fieldset>
			<legend>Ciudad</legend>
			ID: <input type="text" name="identificador" tabindex="1" readonly />
			Nombre de la ciudad: <input type="text" name="nombre" id="nombre" tabindex="2"/>
			Código de la ciudad: <input type="text" name="codigo" id="codigo"/ tabindex="3"/>
			Nombre del distrito: <input type="text" name="distrito" id="distrito" tabindex="4"/>
			Número de habitantes: <input type="number" name="habitantes" id="habitantes" tabindex="5"/>
			<input type="submit" name="guardar" value="Guardar">
			<input type="button" name="cancelar" value="Cancelar">
		</fieldset>
	</form>
<?php } ?>
</body>
</html>