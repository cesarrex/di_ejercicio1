 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<link rel="stylesheet" type="text/css" href="css/list.css">
 	<link rel="stylesheet" type="text/css" href="css/index.css">
 	<title>Index</title>
 </head>
 <body>
 	<h1>Práctica DI</h1>
<?php 
 	session_start();
	if (isset($_SESSION["user"])) {
		echo "<h3>Usuario: ".$_SESSION['user_name']."</h3>";
		echo "<a href='logout.php'>Cerrar sesion</a><br>";
	}else{
		header("Location: login.php");
		exit();
	}
?>
	<br>
	<h4>Formulario para crear registros</h4>
 	<nav>
 		<form action="form.php" method="POST">
 			<input type="hidden" name="ID" value="1">
 			<input type="submit" name="Formulario" value="Formulario">
 		</form>
 	</nav>
 	<br>
 	<?php require "list.php"; ?>
 </body>
 </html>