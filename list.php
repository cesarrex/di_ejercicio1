	
	<?php
	//Paginacion
	$pagina = (isset($_POST['pagina'])? $_POST['pagina'] : 1);
	$num_registros = (isset($_POST['num_registros'])? $_POST['num_registros'] : 10);
	$primero = (isset($_POST['primero'])? true : false);
	$ultimo = (isset($_POST['ultimo'])? true : false);
	$siguiente = (isset($_POST['siguiente'])? true : false);
	$anterior = (isset($_POST['anterior'])? true : false);
	$mostrar = (isset($_POST['mostrar'])? true : false);


	$id=isset($_POST["ID"]) ? $_POST["ID"]:null;
	$name=isset($_POST["Name"]) ? $_POST["Name"]:null;
	$countrycode=isset($_POST["CountryCode"]) ? $_POST["CountryCode"]:null;
	$district=isset($_POST["District"]) ? $_POST["District"]:null;
	$population=isset($_POST["Population"]) ? $_POST["Population"]:null;
	//print_r($_POST);
	try{
		include_once "conf.php";
		$sql="SELECT COUNT(*) FROM city WHERE true";
		$sql_filters = "";
		$filters=array();
		if (!empty($id)){
			$sql.=" and id=:id";
			$filters[":id"]=$id;
		}elseif(!empty($name)){
			$sql.=" and name like :name";
			$filters[":name"]="%$name%";
		}elseif(!empty($countrycode)){
			$sql.=" and countrycode like :countrycode";
			$filters[":countrycode"]="%$countrycode%";
		}elseif(!empty($population)){
			$sql.=" and population like :population";
			$filters[":population"]="%$population%";
		}

		$sql .= $sql_filters;
		$stmt = $con->prepare($sql);
		$stmt->execute($filters);
		$result=$stmt->fetch();
		$total_registros=$result[0];

		//Paginacion
		$paginas=ceil($total_registros/$num_registros);
		if ($primero) $pagina = 1;
		if ($ultimo) $pagina = $paginas;
		if ($siguiente && $pagina<$paginas) $pagina++;
		if ($anterior && $pagina>1) $pagina--;
		if ($mostrar) $pagina = 1;
		//Tabla de registros
		$sql="SELECT * FROM city WHERE true";
		$sql.=$sql_filters;
		$sql.=" LIMIT ".($num_registros*($pagina-1)).",".$num_registros;
		$stmt = $con->prepare($sql);
		$stmt->execute($filters);
		?>
		<script type="text/javascript">
			function eliminar(id){
				alert(id);
				var r = confirm("¿Estás seguro de eliminar "+id+"?");
				if (r==true) {
					console.log("Eliminar: "+id);
					window.location="delete.php?ID="+id;
				}
			}
		</script>
		
		<form action="index.php" id="buscador" method="post">
			<fieldset>
				<legend>Buscador</legend>
					ID: <input type="text" name="ID" value="<?php echo $id?>">
					Nombre: <input type="text" name="Name" value="<?php echo $name?>">
					Codigo del País: <input type="text" name="CountryCode" value="<?php echo $countrycode?>">
					Distrito: <input type="text" name="District" value="<?php echo $district?>">
					Habitantes: <input type="text" name="Population" value="<?php echo $population?>">
					<input type="submit" value="Buscar">
					<input type="reset" value="Limpiar">
			</fieldset>
		</form>
		<table border="1">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>Codigo País</th>
					<th>Distrito</th>
					<th>Habitantes</th>
				</tr>
			</thead>
		<?php 
		/*$filtrado=$sql.=$sql_filters;
		$tabla=$con->prepare($filtrado);*/
		foreach ($stmt as $value) {
			echo "<tr>";
			echo "<td>".$value["ID"]."</td>";
			echo "<td>".$value["Name"]."</td>";
			echo "<td>".$value["CountryCode"]."</td>";
			echo "<td>".$value["District"]."</td>";
			echo "<td>".$value["Population"]."</td>";
			echo "<td><a href='form.php?ID=".$value['ID'].'&Name='.$value['Name'].'&CountryCode='.$value['CountryCode'].'&District='.$value['District'].'&Population='.$value['Population']."'>Modificar</a></td>";
			echo "<td><a onclick='eliminar(".$value['ID'].")'>Eliminar</a></td>";
			echo "</tr>";
		}
		/*while($datos=$stmt->fetch(PDO::FETCH_ASSOC)){
			
		}*/
		?>
		</table>
		<br>
		<div>
			<form action="index.php" method="POST">
				<input type="submit" value="<<" name="primero" /> &nbsp;
				<input type="submit" value="<" name="anterior" /> &nbsp;
				<input type="text" name="pagina" value="<?php echo $pagina?>" /> &nbsp;
				<input type="submit" value=">" name="siguiente" /> &nbsp;
				<input type="submit" value=">>" name="ultimo" /> &nbsp;
				<label for="num_registros">Registros por página: </label>
				<select name="num_registros">
					<option value="10" <?php echo ($num_registros==10? "selected":"")?>>10</option>
					<option value="15" <?php echo ($num_registros==15? "selected":"")?>>15</option>
					<option value="20" <?php echo ($num_registros==20? "selected":"")?>>20</option>
				</select>
				<input type="submit" value="Mostrar" name="mostrar" />
				<br/><br/>
				<span>Núm. Registros: <?php echo $total_registros?></span> &nbsp;
				<span>Página <?php echo $pagina ."/". $paginas?></span>
			</form>
	</div>
		<?php
	}catch (PDOException $e){
		echo "ERROR: ".$e->getMessage();
	}
	?>
